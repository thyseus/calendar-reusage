<?php

echo "Please enter the date of your calendar:\n";

$year = readline();

$days = [
	0 => 'Sunday',
	1 => 'Monday',
	2 => 'Tuesday',
	3 => 'Wednesday',
	4 => 'Thursday',
	5 => 'Friday',
	6 => 'Saturday',
];

$weekdayBase = date('w', strtotime("{$year}-01-01"));
$isLeapYearBase = date('L', strtotime("{$year}-01-01"));

for ($i = $year; $i < $year + 200; $i++) {
	$weekday = date('w', strtotime("{$i}-01-01"));

	if ($weekdayBase == $weekday) {
		$isLeapYear = date('L', strtotime("{$i}-01-01"));
		$leap = $isLeapYear ? 'is a leap year' : 'is not a leap year';
		echo "The first weekday of {$i} is: {$days[$weekday]} and {$leap}\n";
	}
}
