Have an old calendar Hanging on your wall?

Reuse it ! But when?

This little php script calculates in which year you can reuse an outdated calendar. This
is defined as the next year that is _also_ (a/not) leap year and has the same weekday
on first of january.

